// 1.	Design a stack class that supports push, pop, top, and retrieving the maximum element.

// The stack stores integers only.

// push(int x) -- Push element x onto stack.
// pop() -- Removes the element on top of the stack.
// top() -- Get the top element.
// getMax() -- Retrieve the maximum element in the stack.

class Stack {
  constructor() {
    this.array = [];
  }
  push(number) {
    this.array.push(number);
  }
  pop() {
    this.array.pop();
  }
  top() {
    return this.array[this.array.length - 1];
  }
  getMax() {
    return Math.max(...this.array);
  }
}

module.exports = Stack;
