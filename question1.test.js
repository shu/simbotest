const Stack = require("./question1");
let stack = new Stack(); //[]
stack.push(1); //[1]
stack.push(2); //[1,2]
stack.push(-3); //[1,2,-3]
stack.push(7); //[1,2,-3,7]
stack.pop(); //[1,2,-3]

test("stack top method", () => {
  expect(stack.top()).toBe(-3);
});

test("stack getMax method", () => {
  expect(stack.getMax()).toBe(2);
});
