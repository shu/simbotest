// 2.	Given an integer array nums, and it contains two elements that sum up to a specific number target. Design a method (function) to find them.

// You can assume that there is always exactly one solution. The order of the result does not matter.

// Example: 
// Input: nums = [1, 4, 7, 23, 9], and target = 13. 
// Output: {4, 9}, because 4 + 9 = 13.
function sum (nums,target){
  for(i=0;i<nums.length;i++){
    for(t=0;t<nums.length;t++){
        if(i===t){
            break
        }
      if(nums[t]+nums[i]===target){
          return [nums[t],nums[i]]
      }
    }

  }

}
module.exports = sum;
